from os import getcwd
HOST = 'localhost'
DB = 'lims_cn_sw'
USER = '********'
PASSWD = '*******'

ROOT_DIR = getcwd()
TEMPLATE_DIR = f"{ROOT_DIR}/views"

STATES_OF_MOORING = "(17, 18, 20, 21, 22)"
STATES_OF_ROADSTEAD = "(16, 19, 23)"
STATES_OF_ARRIVAL_PREVISION = "(10,)"
STATES_OF_ARRIVALS = "(13,)"
STATES_OF_DEPARTURES = "(26,)"
