import tornado.ioloop
from tornado.web import Application
from handlers.Main import MooredNowHandler


def create_app():
    return Application([
        (r"/moored_now", MooredNowHandler)
    ])


if __name__ == "__main__":
    app = create_app()
    app.listen(8888)
    tornado.ioloop.IOLoop.current().start()
