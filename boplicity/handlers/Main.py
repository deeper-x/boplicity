from tornado.web import RequestHandler
from utils.database import ShipflowDB
from data.configuration import TEMPLATE_DIR, STATES_OF_MOORING


class MooredNowHandler(RequestHandler):
    async def get(self):
        try:
            id_portinformer = self.get_argument("id_portinformer")
            i_date = self.get_argument("i_date")

            db = ShipflowDB()
            interested_states = STATES_OF_MOORING
            self._all_data = None

            async def get_list():
                self._all_data = db.get_moored_now(id_portinformer,
                                                    i_date,
                                                    interested_states)

            await get_list()

            self.render(f"{TEMPLATE_DIR}/all_ships.html", all_ships=self._all_data)

        except Exception as e:
            print("Error getting data: " + str(e))



