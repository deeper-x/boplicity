from data.configuration import HOST, DB, USER, PASSWD
import psycopg2
from utils.QueryTmpl import QueryTmpl
from utils.queries_tmpl import q_moored_now
from time import sleep

class ShipflowDB:
    def __init__(self):
        self._engine = psycopg2.connect(f'dbname={DB} user={USER} password={PASSWD}')
        self._cursor = self._engine.cursor()
        self._query_tmpl = QueryTmpl()

    def __repr__(self):
        return f"<ShipflowDB engine={self._engine}>"

    def get_moored_now(self, id_portinformer, i_date, interested_states):
        self._query_tmpl.str_query = q_moored_now

        query = self._query_tmpl.str_query.format(id_portinformer=id_portinformer,
                                                  i_date=i_date,
                                                  interested_states=interested_states)

        self._cursor.execute(query)

        return self._cursor.fetchall()